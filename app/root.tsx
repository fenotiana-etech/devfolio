import type {
  LinksFunction,
  LoaderFunctionArgs,
  MetaFunction,
} from "@remix-run/node";
import { json, type SerializeFrom } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";

import tailwindStylesheetUrl from "./styles/tailwind.css";

import siteWebManifest from "../public/site.webmanifest";
import safariPinnedTab from "../public/safari-pinned-tab.svg";
import icon16 from "../public/favicon-16x16.png";
import icon32 from "../public/favicon-32x32.png";
import appleTouchIcon from "../public/apple-touch-icon.png";
import { getServerTimeHeader } from "./utils/timing.server.ts";
import { getDomainUrl } from "./utils/misc.tsx";
import { getTheme } from "./utils/theme.server.ts";
import { type KCDHandle } from "./types.ts";

export const links: LinksFunction = () => {
  return [
    { rel: "stylesheet", href: tailwindStylesheetUrl },
    { rel: "manifest", href: siteWebManifest },
    { rel: "mask-icon", href: safariPinnedTab, color: "#5bbad5" },
    { rel: "icon", type: "image/png", sizes: "32x32", href: icon32 },
    { rel: "icon", type: "image/png", sizes: "16x16", href: icon16 },
    { rel: "apple-touch-icon", sizes: "180x180", href: appleTouchIcon },
  ];
};

const title = "Fenotiana Andriamahenimanana - Développeur Javascript";
const description =
  "Développeur Javascript avec plus de 5ans d'experience, je peut à la fois occuper le rôle d'un développeur frontend et backend. J'utilise principalement Remix et React";
const url = "https://fenotiana.dev";
const image = "https://avatars.githubusercontent.com/u/87015111?v=4";

const keywords = [
  "Développeur Javascript",
  "Développeur Typescript",
  "Développeur React",
  "Développeur React Tana",
  "Développeur React Antananarivo",
  "Développeur Remix",
  "tailwindcss",
  "Développeur JavaScript Mada",
  "Développeur JavaScript Madagascar",
  "Développeur JavaScript Antananarivo",
  "Développeur JavaScript Tana",
  "Développeur Expérimenté",
];

export const meta: MetaFunction = () => [
  { title },
  { name: "description", content: description },
  { charSet: "utf-8" },
  { name: "viewport", content: "width=device-width,initial-scale=1" },
  {
    name: "keywords",
    content: keywords.join(","),
  },
  { name: "og:type", content: "website" },
  { name: "og:url", content: url },
  { name: "og:title", content: title },
  { name: "og:image", content: image },
  { name: "og:description", content: description },
  { name: "twitter:card", content: "summary_large_image" },
  { name: "twitter:creator", content: "@MansAndrIam" },
  { name: "twitter:url", content: url },
  { name: "twitter:title", content: title },
  { name: "twitter:description", content: description },
  { name: "twitter:image", content: image },
  { name: "msapplication-TileColor", content: "#da532c" },
  { name: "theme-color", content: "#ffffff" },
];

export const handle: KCDHandle & { id: string } = {
  id: "root",
};

export type RootLoaderType = typeof loader;

export type LoaderData = SerializeFrom<typeof loader>;

export async function loader({ request }: LoaderFunctionArgs) {
  const timings = {};
  const data = {
    requestInfo: {
      // hints: getHints(request),
      origin: getDomainUrl(request),
      path: new URL(request.url).pathname,
      userPrefs: {
        theme: getTheme(request),
      },
    },
  };

  const headers: HeadersInit = new Headers();
  headers.append("Server-Timing", getServerTimeHeader(timings));

  return json(data, { headers });
}

export default function App() {
  return (
    <html className="h-full">
      <head>
        <Meta />
        <Links />
      </head>
      <body className="h-full text-gray-700 transition-all duration-300 dark:text-white ">
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
