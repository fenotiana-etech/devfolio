#!/bin/sh
"which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
mkdir -p ~/.ssh
echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/gitlab
chmod 700 ~/.ssh/gitlab
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/gitlab
ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
apt-get install rsync -y -qq
apt-get install curl -y -qq