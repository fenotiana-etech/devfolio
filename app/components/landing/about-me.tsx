import { useTranslation } from "react-i18next";
import Title from "../title";
import Container from "./container";
import type { StackProps } from "../stack";
import StackListing from "../stack";

import ReactIcon from "../icon/react";
import TypescriptIcon from "../icon/typescript";
import ViteIcon from "../icon/vite";
import TailwindcssIcon from "../icon/tailwindcss";
import NodejsIcon from "../icon/nodejs";
import PostgresIcon from "../icon/postgresql";
import AngularIcon from "../icon/angular";
import ApacheIcon from "../icon/apache";
import AwsIcon from "../icon/aws";
import DockerIcon from "../icon/docker";
import MongoIcon from "../icon/mongo";
import MySqlIcon from "../icon/mysql";
import TerraformIcon from "../icon/terraform";

const preferedStack: StackProps["stacks"] = [
  { image: <ReactIcon />, href: "https://reactjs.org/" },
  { image: <TypescriptIcon />, href: "https://www.typescriptlang.org/" },
  { image: <ViteIcon />, href: "https://vitejs.dev/" },
  { image: <TailwindcssIcon />, href: "https://tailwindcss.com/" },
  { image: <NodejsIcon />, href: "https://nodejs.org/" },
  { image: <PostgresIcon />, href: "https://www.postgresql.org/" },
];

const otherStack: StackProps["stacks"] = [
  { image: <AngularIcon />, href: "https://angular.io/" },
  { image: <ApacheIcon />, href: "https://httpd.apache.org/" },
  { image: <AwsIcon />, href: "https://aws.amazon.com/" },
  { image: <DockerIcon />, href: "https://www.docker.com/" },
  { image: <MongoIcon />, href: "https://www.mongodb.com//" },
  { image: <MySqlIcon />, href: "https://www.mysql.com/" },
  { image: <TerraformIcon />, href: "https://www.terraform.io/" },
];

export default function AboutMe() {
  const { t } = useTranslation();
  return (
    <Container className="!h-fit flex-col p-8">
      <Title>{t("aboutMe.title")}</Title>

      <p>{t("aboutMe.biography")}</p>

      <StackListing
        title={t("aboutMe.favouriteStacks")}
        stacks={preferedStack}
      />
      <StackListing title={t("aboutMe.otherStacks")} stacks={otherStack} />
    </Container>
  );
}
