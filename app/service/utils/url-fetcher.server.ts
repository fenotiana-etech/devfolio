import { fetch } from "@remix-run/node";

export const urlFetcher = async (url: string | undefined) => {
  console.log("urlFetcher", url);
  if (!url) {
    return null;
  }
  try {
    const response = await fetch(url);
    const html = await response.text();

    return html;
  } catch (error) {
    console.log(error);
    return null;
  }
};
