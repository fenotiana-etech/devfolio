These utils are borrowed from

- cache.server.ts
- compile-mdx.server.ts
- github.server.ts
- markdown.server.ts
- mdx.tsx
- mdx.server.ts
- misc.tsx
- timing.server.ts
- twitter.server.ts
- use-root-data.ts
- theme.server.ts

https://github.com/kentcdodds/kentcdodds.com/tree/main/app/utils
