/** @type {import('tailwindcss').Config} */
export default {
  darkMode: "class",
  content: ["./app/**/*.{ts,tsx,jsx,js}"],
  theme: {
    extend: {
      backgroundImage: {
        "landing-bg": "url('/images/bg.svg')",
        "landing-bg-dark": "url('/images/bg-dark.svg')",
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
